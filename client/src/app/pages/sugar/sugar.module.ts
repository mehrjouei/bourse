import { NgModule } from '@angular/core';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { SugarRoutingModule, routedComponents } from './sugar-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    SugarRoutingModule,
    ToasterModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class SugarModule { }
