import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'داشبورد',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'صنعت قند',
    icon: 'nb-gear',
    children: [
     {
        title: 'گزارشات',
        link: '/pages/sugar/list',
      },
     {
        title: 'ثبت گزارش',
        link: '/pages/sugar/createReport',
      },
    ],
  }
];
