import { Component } from '@angular/core';

@Component({
  selector: 'ngx-sugar',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SugarComponent {
}
