import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SugarComponent } from './sugar.component';
import { ListComponent } from './list/list.component';
import { CreateReportComponent } from './createReport/createReport.component';

const routes: Routes = [{
  path: '',
  component: SugarComponent,
  children: [
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'createReport',
    component: CreateReportComponent,
  },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SugarRoutingModule { }

export const routedComponents = [
  SugarComponent,
  ListComponent,
  CreateReportComponent
];
